//
//  RecipeDetailViewController.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 11/08/23.
//

import UIKit

class RecipeDetailViewController: UIViewController {

    private let viewModel = RecipeViewModelImp()
    private var recipeName: String?
    var recipeID: String?
    var recipeImage: UIImage?
    var index: Int?
    private let ingredientsCellID = "ingredientsCell"
    private let instructionsCellID = "instructionsCell"
    private let upperView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 1, alpha: 0.6)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let labelView: UIView = {
        let view = UIView()
        view.backgroundColor = customColor2
        view.layer.cornerRadius = 18
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = customColor2
        view.layer.cornerRadius = 25
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private var mealImageView = UIImageView()
    private let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.5
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let subtitleLabel: UILabel = {
        let lbl = UILabel()
        
        lbl.text = "Desserts"
        lbl.font = UIFont(name: "Al Bayan", size: 20)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
    return lbl
}()
    lazy var recipeTableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        tableView.showsVerticalScrollIndicator = false
        tableView.register(IngredientsTableViewCell.self, forCellReuseIdentifier: self.ingredientsCellID)
        tableView.register(InstructionsTableViewCell.self, forCellReuseIdentifier: self.instructionsCellID)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = customColor2
        viewModel.delegate = self
        if let recipeID = recipeID{
            viewModel.getMealDetails(id: recipeID)
        }
        setUpImages()
        setUpViewComponents()
    }
    func setUpImages(){
                
        mealImageView.translatesAutoresizingMaskIntoConstraints = false
        mealImageView.image = recipeImage
        mealImageView.backgroundColor = .blue
        mealImageView.contentMode = .scaleAspectFit
        mealImageView.clipsToBounds = true
    }
    
    func setUpViewComponents(){
        
        view.addSubview(mealImageView)
        view.addSubview(labelView)
        view.addSubview(contentView)
        view.addSubview(upperView)

        view.addSubview(titleLabel)
        view.addSubview(lineView)
        view.addSubview(subtitleLabel)
        view.addSubview(recipeTableView)

        
        NSLayoutConstraint.activate([
            
            mealImageView.topAnchor.constraint(equalTo: view.topAnchor),
            mealImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mealImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mealImageView.heightAnchor.constraint(equalToConstant: view.bounds.width),
            
            labelView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: -38),
            labelView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0),
            labelView.widthAnchor.constraint(equalToConstant: 85),
            labelView.heightAnchor.constraint(equalToConstant: 34),
            
            upperView.topAnchor.constraint(equalTo: view.topAnchor),
            upperView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            upperView.heightAnchor.constraint(equalToConstant: 60),
            upperView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            contentView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 270),
            contentView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 0),
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 70),
            titleLabel.widthAnchor.constraint(equalToConstant: 250),
            
            lineView.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 50),
            lineView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 2),
            lineView.widthAnchor.constraint(equalToConstant: 150),
            
            subtitleLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 45),
            subtitleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            subtitleLabel.heightAnchor.constraint(equalToConstant: 40),
            subtitleLabel.widthAnchor.constraint(equalToConstant: 100),
                        
            recipeTableView.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 80),
            recipeTableView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            recipeTableView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            recipeTableView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        ])
    }

}

extension RecipeDetailViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return viewModel.ingredientsArray.count

        }else{
            return 1
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = recipeTableView.dequeueReusableCell(withIdentifier: ingredientsCellID, for: indexPath) as! IngredientsTableViewCell
            cell.ingredientText = viewModel.ingredientsArray[indexPath.row]
            cell.measureText = viewModel.measuresArray[indexPath.row]
            return cell
        }else{
            let cell = recipeTableView.dequeueReusableCell(withIdentifier: instructionsCellID, for: indexPath) as! InstructionsTableViewCell
            cell.instructionText = viewModel.instructionsString
            print(cell.instructionText)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 40
        }else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        headerView.backgroundColor = customColor3
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 30))
        titleLabel.textAlignment = .center
        titleLabel.textColor = UIColor.black
        if section == 0{
            titleLabel.text = "Ingredients"
        }else{
            titleLabel.text = "Instructions"
        }
        
        
        headerView.addSubview(titleLabel)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
}

extension RecipeDetailViewController: RecipeViewModel{
    func reloadTVData(name: String?){
        recipeName = name
        DispatchQueue.main.async {
            if let font = UIFont(name: "Helvetica Neue", size: 22){
                let attributedText = NSMutableAttributedString(string: self.recipeName ?? "")
                let range = NSRange(location: 0, length: attributedText.length)
                attributedText.addAttribute(.font, value: font, range: range)
                self.titleLabel.attributedText = attributedText
            }
            self.recipeTableView.reloadData()
        }
    }
}
