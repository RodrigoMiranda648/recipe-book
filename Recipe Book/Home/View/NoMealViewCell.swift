//
//  NoMealViewCell.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 08/08/23.
//

import UIKit

class NoMealViewCell: UITableViewCell {

    let noMealLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Algo salió mal al obtener los datos"
        lbl.font = UIFont.systemFont(ofSize: 17)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setUpNoMealCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpNoMealCell(){
        self.addSubview(noMealLabel)

        NSLayoutConstraint.activate([
            noMealLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            noMealLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            noMealLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            noMealLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10)
        ])
    }

}
