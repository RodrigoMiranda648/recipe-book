//
//  HomeModel.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 08/08/23.
//

import Foundation
import UIKit

struct MealListClientResponse: Decodable{
    var meals: [Meal]
}
struct Meal: Decodable{
    var strMeal: String
    var strMealThumb: String
    var idMeal: String
}

