//
//  HomeViewModelImp.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 08/08/23.
//

import Foundation
import UIKit

protocol HomeViewModel: NSObject{
    func didImageFinishLoad()
}

class HomeViewModelImp: NSObject {
    
    weak var delegate: HomeViewModel?
    var meals: [Meal] = []
    var finishedImageLoad = 0
    var mealsImages: [UIImage?] = []{
        didSet{
            if finishedImageLoad >= meals.count - 1{
                delegate?.didImageFinishLoad()
            }
        }
    }
    let navigationController: UINavigationController?
    
    //MARK: INIT
    init(navigationController: UINavigationController?){
        self.navigationController = navigationController
    }
    
    func getMeals(){
        NetworkServices.shared.getData(url: "https://themealdb.com/api/json/v1/1/filter.php?c=Dessert") { res, error in
            if let result = res{
                let decodedResult: Result<MealListClientResponse, Error> = NetworkServices.shared.decodeJSON(data: result, type: MealListClientResponse.self)
                switch decodedResult {
                case .success(let decodedData):
                    self.processMeals(data: decodedData)
                case .failure(let error):
                    print("Error decoding JSON: \(error)")
                }
            }
            
        }
    }
    
    func processMeals(data: MealListClientResponse){
        self.meals = data.meals
        for _ in 0...self.meals.count - 1{
            self.mealsImages.append(nil)
        }
        self.getImages()
    }
        

    
    
    func getImages(){
        for index in 0...meals.count - 1{
            GetImageService.shared.getImages(meal: meals[index], completionIndex: index) { res, completionIndex in
                self.mealsImages[completionIndex] = res
                self.finishedImageLoad += 1
                
            }
        }
    }
    
    func openDetaiView(mealID: Int, image: UIImage?){
        
        let recipeDetailVC = RecipeDetailViewController()
        recipeDetailVC.recipeID = meals[mealID].idMeal
        recipeDetailVC.index = mealID
        recipeDetailVC.recipeImage = image
        self.navigationController?.pushViewController(recipeDetailVC, animated: true)
    }
}
