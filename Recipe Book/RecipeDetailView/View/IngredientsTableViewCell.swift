//
//  IngredientsTableViewCell.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 14/08/23.
//

import UIKit

class IngredientsTableViewCell: UITableViewCell {
    
    let ingredientLabel: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        lbl.font = UIFont.systemFont(ofSize: 17)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.5
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let measureLabel: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        lbl.font = UIFont.systemFont(ofSize: 17)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.5
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    var ingredientText: String?{
        didSet{
            ingredientLabel.text = ingredientText
        }
    }
    var measureText: String?{
        didSet{
            measureLabel.text = measureText
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        setUpIngredientCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpIngredientCell(){
        self.addSubview(ingredientLabel)
        self.addSubview(measureLabel)

        NSLayoutConstraint.activate([
            ingredientLabel.topAnchor.constraint(equalTo: self.topAnchor),
            ingredientLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            ingredientLabel.widthAnchor.constraint(equalToConstant: 180),
            ingredientLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            
            measureLabel.topAnchor.constraint(equalTo: self.topAnchor),
            measureLabel.widthAnchor.constraint(equalToConstant: 180),
            measureLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            measureLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }

}
