//
//  HomeViewController.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 08/08/23.
//

import UIKit

let customColor1 = #colorLiteral(red: 0.3058823529, green: 0.3490196078, blue: 0.5529411765, alpha: 1)
let customColor2 = #colorLiteral(red: 1, green: 0.937254902, blue: 0.8666666667, alpha: 1)
let customColor3 = #colorLiteral(red: 0.9764705882, green: 0.7843137255, blue: 0.5176470588, alpha: 1)
let customColor4 = #colorLiteral(red: 0.9921568627, green: 0.6901960784, blue: 0.3450980392, alpha: 1)
let customColor5 = #colorLiteral(red: 1, green: 0.5490196078, blue: 0.2588235294, alpha: 1)

class HomeViewController: UIViewController {
    
    private var viewModel: HomeViewModelImp?
    let cellID = "meal"
    let noMealCellID = "noMeal"
    var didFinishLoad = false
    
    private var backgroundImageView = UIImageView()
    private var fetchImageView = UIImageView()
    private let upperView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 1, alpha: 0.6)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let menuView: UIView = {
        let view = UIView()
        view.backgroundColor = customColor2
        view.layer.cornerRadius = 5
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 5, height: 5)
        view.layer.shadowRadius = 5
        view.layer.masksToBounds = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let titleLabel: UILabel = {
        let lbl = UILabel()
        
        if let font = UIFont(name: "Helvetica Neue", size: 22){
            let attributedText = NSMutableAttributedString(string: "Cook Book")
            let range = NSRange(location: 0, length: attributedText.length)
            attributedText.addAttribute(.font, value: font, range: range)
            lbl.attributedText = attributedText
        }
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    private let lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private let subtitleLabel: UILabel = {
        let lbl = UILabel()
        
        lbl.text = "Desserts"
        lbl.font = UIFont(name: "Al Bayan", size: 20)
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
    return lbl
}()
    lazy var tableView: UITableView = {
        let table = UITableView()
        
        table.backgroundColor = .clear
        table.register(MealTableViewCell.self, forCellReuseIdentifier: cellID)
        table.register(NoMealViewCell.self, forCellReuseIdentifier: noMealCellID)
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        table.showsVerticalScrollIndicator = false
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = HomeViewModelImp(navigationController: self.navigationController)
        viewModel?.delegate = self
        viewModel?.getMeals()
        setUpImages()
        setUpViewComponents()
    }
    
    func setUpImages(){
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.image = UIImage(named: "table")
        backgroundImageView.contentMode = .scaleAspectFit
        
        fetchImageView.translatesAutoresizingMaskIntoConstraints = false
        fetchImageView.image = UIImage(named: "fetch")
        fetchImageView.contentMode = .scaleAspectFit
    }
    
    func setUpViewComponents(){
        
        view.addSubview(backgroundImageView)
        view.addSubview(upperView)
        view.addSubview(menuView)
        view.addSubview(fetchImageView)
        view.addSubview(titleLabel)
        view.addSubview(lineView)
        view.addSubview(subtitleLabel)
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            
            backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            upperView.topAnchor.constraint(equalTo: view.topAnchor),
            upperView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            upperView.heightAnchor.constraint(equalToConstant: 60),
            upperView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            menuView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 80),
            menuView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -30),
            menuView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 30),
            menuView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -30),
            
            fetchImageView.topAnchor.constraint(equalTo: menuView.topAnchor,constant: 10),
            fetchImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            fetchImageView.heightAnchor.constraint(equalToConstant: 50),
            
            titleLabel.topAnchor.constraint(equalTo: menuView.topAnchor,constant: 40),
            titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 70),
            titleLabel.widthAnchor.constraint(equalToConstant: 150),
            
            lineView.topAnchor.constraint(equalTo: menuView.topAnchor,constant: 90),
            lineView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            lineView.heightAnchor.constraint(equalToConstant: 2),
            lineView.widthAnchor.constraint(equalToConstant: 150),
            
            subtitleLabel.topAnchor.constraint(equalTo: menuView.topAnchor,constant: 85),
            subtitleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            subtitleLabel.heightAnchor.constraint(equalToConstant: 40),
            subtitleLabel.widthAnchor.constraint(equalToConstant: 100),
            
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 200),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -70),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 50),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -50)
        ])
    }
}

extension HomeViewController: HomeViewModel{
    
    func didImageFinishLoad(){
        didFinishLoad = true
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if didFinishLoad{
            return viewModel?.meals.count ?? 0
        }else{
            
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if didFinishLoad{
            let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! MealTableViewCell
            if let _ = viewModel?.mealsImages[indexPath.row]{
                cell.meal = viewModel?.meals[indexPath.row]
                cell.mealImage = viewModel?.mealsImages[indexPath.row]
            }
            
            if indexPath.row % 2 == 0{
                cell.setUpMealCell(isTheCellPar: true)
            }else{
                cell.setUpMealCell(isTheCellPar: false)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: noMealCellID, for: indexPath) as! NoMealViewCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedColorView = UIView()
        selectedColorView.backgroundColor = .clear
        tableView.cellForRow(at: indexPath)?.selectedBackgroundView = selectedColorView
        if let cell = tableView.cellForRow(at: indexPath) as? MealTableViewCell {
            let selectedImage = cell.mealImage
            viewModel?.openDetaiView(mealID: indexPath.row, image: selectedImage)
        }
    }
}

