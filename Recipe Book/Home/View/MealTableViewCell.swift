//
//  MealTableViewCell.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 08/08/23.
//

import UIKit

class MealTableViewCell: UITableViewCell {
        
    let backGroundView: UIView = {
        let view = UIView()
        view.backgroundColor = customColor3
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let strMealLabel: UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = .clear
        lbl.font = UIFont.systemFont(ofSize: 20)
        lbl.numberOfLines = 0
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.5
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    var mealImageView = UIImageView()

    var meal: Meal?{
        didSet{
            strMealLabel.text = self.meal?.strMeal
//            strMealThumbLabel.text = self.meal?.strMealThumb
        }
    }
    var mealImage: UIImage?{
        didSet{
            mealImageView.image = mealImage
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        mealImageView.translatesAutoresizingMaskIntoConstraints = false
        mealImageView.contentMode = .scaleAspectFit
        mealImageView.layer.cornerRadius = 15
        mealImageView.clipsToBounds = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func disableConstraints(){
        for view in self.subviews{
            view.removeFromSuperview()
        }
    }
    
    func setUpMealCell(isTheCellPar: Bool){
        disableConstraints()
        
        self.backgroundColor = .clear
        self.addSubview(backGroundView)
        self.addSubview(strMealLabel)
        self.addSubview(mealImageView)
        
        NSLayoutConstraint.activate([
            backGroundView.topAnchor.constraint(equalTo: self.topAnchor, constant: 5),
            backGroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0),
            backGroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -0),
            backGroundView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5),
            
        ])
        if isTheCellPar{
            strMealLabel.textAlignment = .center
            NSLayoutConstraint.activate([
                
                mealImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                mealImageView.widthAnchor.constraint(equalToConstant: 80),
                mealImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -5),
                mealImageView.heightAnchor.constraint(equalToConstant: 80),
                
                strMealLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                strMealLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
                strMealLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -100),
                strMealLabel.heightAnchor.constraint(equalToConstant: 50)])
        }else{
            strMealLabel.textAlignment = .center
            NSLayoutConstraint.activate([
                
                mealImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                mealImageView.widthAnchor.constraint(equalToConstant: 80),
                mealImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 5),
                mealImageView.heightAnchor.constraint(equalToConstant: 80),
                
                strMealLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                strMealLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 100),
                strMealLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
                strMealLabel.heightAnchor.constraint(equalToConstant: 50),
                
            ])
        }
    }
}
