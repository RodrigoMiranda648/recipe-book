//
//  RecipeTableViewCell.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 14/08/23.
//

import UIKit

class InstructionsTableViewCell: UITableViewCell {

    private let instructionsTextView: UITextView = {
        let text = UITextView()
        
        text.font = .systemFont(ofSize: 20)
        text.backgroundColor = .clear
        text.textColor = UIColor.black
        text.textAlignment = .justified
        text.clipsToBounds = true
        text.isScrollEnabled = false
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    var instructionText: String?{
        didSet{
            instructionsTextView.text = instructionText
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        instructionsTextView.sizeToFit()
        setUpViewComponents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpViewComponents(){
        
        self.addSubview(instructionsTextView)
        
        NSLayoutConstraint.activate([
            instructionsTextView.topAnchor.constraint(equalTo: self.topAnchor),
            instructionsTextView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            instructionsTextView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            instructionsTextView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }

}
