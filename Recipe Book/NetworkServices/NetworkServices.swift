//
//  NetworkServices.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 08/08/23.
//

import Foundation
import UIKit

class NetworkServices {
    
    //Singleton
    static var shared: NetworkServices = NetworkServices()
    
    init(){
        
    }
    
    func getData(url: String, completion: @escaping(Data?, Error?) -> ()){
        let urlString: String = url
        guard let url = URL(string: urlString) else {return}

        URLSession.shared.dataTask(with: url){data, response, error in
            if let error = error{
                print(error.localizedDescription)
                completion(nil, error)
            }
            if let data = data{
                completion(data, nil)
            }

        }.resume()
    }
    func decodeJSON<T: Decodable>(data: Data, type: T.Type) -> Result<T, Error>{
        
        do{
            let objects = try JSONDecoder().decode(type.self, from: data)
            return .success(objects)
        }catch{
            print(error.localizedDescription)
            return .failure(error)
        }
    }
}



class GetImageService {
    static var shared: GetImageService = GetImageService()
    init(){
        
    }
    
    func getImages(meal: Meal, completionIndex: Int,completion: @escaping(UIImage?, Int) -> ()){
        let index = completionIndex
        let urlString = String(meal.strMealThumb)
        guard let url = URL(string: urlString)else{return}
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error{
                print("error:")
                print(error.localizedDescription)
                completion(nil, index)
            }
            if let data = data{
                let image = UIImage(data: data)
                completion(image ?? UIImage(), index)
            }
        }.resume()
    }
}
