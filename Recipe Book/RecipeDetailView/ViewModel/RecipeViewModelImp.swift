//
//  RecipeViewModelImp.swift
//  Recipe Book
//
//  Created by Rodrigo Miranda Castillo on 13/08/23.
//

import Foundation

protocol RecipeViewModel: NSObject{
    func reloadTVData(name: String?)
}

class RecipeViewModelImp: NSObject{
    weak var delegate: RecipeViewModel?
    let partialURL = "https://themealdb.com/api/json/v1/1/lookup.php?i="
    var ingredientsArray: [String?]
    var measuresArray: [String?]
    var instructionsString: String?
    
    override init(){
        ingredientsArray = []
        measuresArray = []
    }
    
    func getMealDetails(id: String){
        let url = partialURL + id
        NetworkServices.shared.getData(url: url) { res, error in
            if let result = res{
                let decodedResult: Result<MealClientResponse, Error> = NetworkServices.shared.decodeJSON(data: result, type: MealClientResponse.self)
                switch decodedResult {
                case .success(let decodedData):
                    self.processMealDetails(data: decodedData)
                case .failure(let error):
                    print("Error decoding JSON: \(error)")
                }
            }
        }
    }
    
    func getIngredientsArray(data: MealClientResponse) {
        if let str = data.meals[0].strIngredient1 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient2 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient3 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient4 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient5 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient6 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient7 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient8 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient9 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient10 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient11 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient12 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient13 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient14 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient15 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient16 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient17 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient18 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient19 { if str != ""{ ingredientsArray.append(str)}}
        if let str = data.meals[0].strIngredient20 { if str != ""{ ingredientsArray.append(str)}}
        
        if let str = data.meals[0].strMeasure1{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure2{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure3{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure4{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure5{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure6{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure7{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure8{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure9{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure10{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure11{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure12{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure13{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure14{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure15{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure16{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure17{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure18{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure19{ if str != ""{ measuresArray.append(str)}}
        if let str = data.meals[0].strMeasure20{ if str != ""{ measuresArray.append(str)}}
        self.delegate?.reloadTVData(name: data.meals[0].strMeal)
    }
    
    func processMealDetails(data: MealClientResponse){
        instructionsString = data.meals[0].strInstructions
        getIngredientsArray(data: data)
    }
}
